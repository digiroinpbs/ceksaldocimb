package id.corechain

import com.rethinkdb.RethinkDB
import com.rethinkdb.gen.exc.ReqlDriverError
import com.rethinkdb.net.Connection
import org.apache.commons.codec.digest.DigestUtils
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.support.ui.WebDriverWait
import java.util.*
import redis.clients.jedis.Jedis
import redis.clients.jedis.exceptions.JedisConnectionException

class WebDriver(){
    fun getWebDriver(): WebDriver? {
        val props = Properties()
        props.load(this.javaClass.getResourceAsStream("/path.properties"))
        var driver: WebDriver
        System.setProperty("webdriver.chrome.driver", props.get("webdriver.chrome.driver").toString())
        driver = ChromeDriver()
        return driver
    }
}

fun main(args: Array<String>) {
    var result:String
    try {
        val r:RethinkDB = RethinkDB.r;
        val conn:Connection = r.connection().hostname("rth0.corechain.id").port(28015).db("giroin").user("digiro","digiro").connect()
        conn.close()
        val jedis = Jedis("104.131.106.90",6379)
        jedis.auth("LCbpzEw2Zx5BNJaa63v9ivrSUIWzzjv9")
        jedis.select(0)
        jedis.quit()
        result= checkSaldo("alabde25","Corechain123")
    }catch (e:ReqlDriverError){
        System.out.println("connection rethink DB error")
    }catch (e: JedisConnectionException){
        System.out.println("connection redis error")
    }catch (e:Exception){
        e.printStackTrace()
    }
    return
}

fun checkSaldo(username:String, password:String):String{
    val jedis = Jedis("104.131.106.90",6379)
    jedis.auth("LCbpzEw2Zx5BNJaa63v9ivrSUIWzzjv9")
    jedis.select(0)
    var result =""
    val webDriver =id.corechain.WebDriver().getWebDriver()
    val filename = "logCheckBalanceCIMB.txt"
    if(webDriver!=null){
        webDriver.manage().window().maximize();
        webDriver.get("https://www.cimbclicks.co.id/ib-cimbniaga/Login.html")
        var element = webDriver.findElement(By.name("CAPTCHA"))
        val wait = WebDriverWait(webDriver, 100000)
        wait.until(ExpectedConditions.attributeToBeNotEmpty(element,"value"));
        element = webDriver.findElement(By.name("j_plain_username"))
        element.sendKeys(username)
        webDriver.findElement(By.linkText("Login")).click()
        element = webDriver.findElement(By.name("j_plain_password"))
        element.sendKeys(password)

        webDriver.findElement(By.xpath("//button[@type='submit']")).click()

        webDriver.switchTo().defaultContent()
        var frame = webDriver.findElement(By.name("contentframe"))
        webDriver.switchTo().frame(frame)
        webDriver.findElement(By.linkText("Manage My Accounts")).click()
        webDriver.findElement(By.linkText("Account Inquiry")).click()

        var table_element = webDriver.findElement(By.xpath("//table[@class='hidden']//tbody//tr[@class='tableoddrow']//td//table//tbody//tr//td//a"))
        var saldo =table_element.text
        var now =System.currentTimeMillis() / 1000L
        result=result+"{\"bank\":\"CIMB\",\"amount\":\""+saldo+"\",\"lastupdate\":\""+now+"\"}"
        jedis.set(username+":CIMB:"+now,result)
        jedis.set(username+":CIMB:lastest",result)

        result ="result :["
        webDriver.findElement(By.linkText("View/Download Transactions")).click()

        var sel = Select(webDriver.findElement(By.xpath("//select[@id='VIEW_HISTORY']")))
        sel.selectByIndex(7)
        sel = Select(webDriver.findElement(By.xpath("//select[@id='TRANSACTION_ORDER']")))
        sel.selectByIndex(0)
        webDriver.findElement(By.xpath("(//img[@src='/sib-cimbniaga/images/e/butt_view_on_screen.gif'])[2]")).click()
        var tr_collection:List<WebElement>
        try {
            tr_collection = webDriver.findElements(By.xpath("//table[@id='tableView']//tbody//tr"))
        }catch (e:Exception){
            result =result+"]"
            return  result
        }

        var row_num: Int
        var col_num: Int
        row_num = 1
        var first =true;
        var counter=0
        for (trElement in tr_collection) {
            val td_collection = trElement.findElements(By.xpath("td"))
            if(row_num<=2){
                row_num++
                counter++
                continue
            }else if(row_num==tr_collection.size){
                break
            }else if(!first){
                result=result+","
            }
            first=false
            col_num = 1
            var res=""
            var date=""
            var desc=""
            var type=""
            var amount=""
            var indicator:String=""
            for (tdElement in td_collection) {
                if(td_collection.size==1){
                    continue
                }
                if(col_num==1){
                    res=res+"{\"Tanggal\":\""+tdElement.text+"\","
                    date=tdElement.text
                }else if(col_num==2){
                    res=res+"\"Keterangan Transaksi\":\""+tdElement.text+"\","
                    desc=tdElement.text
                }else if(col_num==3){
                    res=res+"\"Debet\":\""+tdElement.text+"\","
                    type="out"
                    amount=tdElement.text
                }else if(col_num==4){
                    res=res+"\"Kredit\":\""+tdElement.text+"\","
                    if(amount.equals("")){
                        type="in"
                        amount=tdElement.text
                    }
                }else if(col_num==5){
                    res=res+"\"Saldo\":\""+tdElement.text+"\"}"
                }
                col_num++
            }

            if(td_collection.size>1){
                var dates =date.split("-")
                var amounts = amount.split(",")
                amount=amounts[0]
                var month =""
                if(dates[1].equals("Jan")){
                    month="01"
                }else if(dates[1].equals("Feb")){
                    month="02"
                }else if(dates[1].equals("Mar")){
                    month="03"
                }else if(dates[1].equals("Apr")){
                    month="04"
                }else if(dates[1].equals("May")){
                    month="05"
                }else if(dates[1].equals("Jun")){
                    month="06"
                }else if(dates[1].equals("Jul")){
                    month="07"
                }else if(dates[1].equals("Aug")){
                    month="08"
                }else if(dates[1].equals("Sep")){
                    month="09"
                }else if(dates[1].equals("Oct")){
                    month="10"
                }else if(dates[1].equals("Nov")){
                    month="11"
                }else if(dates[1].equals("Dec")){
                    month="12"
                }
                date=dates[2]+month+dates[0]
                val r: RethinkDB = RethinkDB.r;
                val conn: Connection = r.connection().hostname("rth0.corechain.id").port(28015).db("giroin").user("digiro","digiro").connect()
                r.table("mutasi").insert(r.hashMap("id", sha1converter(res)).with("date",date.toInt()).with("desc",desc).with("type",type).with("amount",amount).with("processed",-1).with("bank","Maybank")
                ).run<String>(conn)
                conn.close()
            }
            result=result+res;
            row_num++
            counter++
        }
        result =result+"]"

        webDriver.findElement(By.xpath("//area")).click()

        jedis.quit()
        webDriver.quit()
    }
    return result;
}

fun sha1converter(param: String): String {
    return DigestUtils.sha1Hex(param)
}
